# 1) Faça programas que:

# a) Leia um número e imprima o seu quadrado.  FUNCIONANDO

a = int(input('escreva seu número: '))

a = a**2

print('o quadrado do número é: {}'.format(a))


# b) Leia dois números e imprima a divisão do primeiro pelo segundo. FUNCIONANDO

a = int(input('escreva seu primeiro número: '))
b = int(input('escreva seu segundo número: '))


divi = int(a/b)

print('a divisão do primeiro pelo segundo é: {}'.format(divi))

# C) Leia um número e imprima o resto da divisão desse número por 2.  FUNCIONANDO

a = int(input('digite seu número: '))

a = a % 2

print('o resto da divisão de a é: {}'.format(a))


# D) Leia dois número e imprima a média. FUNCIONANDO

a = int(input('digite o primeiro número:'))
b = int(input('digite o segundo número: '))

c = float((a+b)/2)

print('a média é:{}'.format(c))


""" e) Calcule a área de uma circunferência considerando que o usuário informe o
comprimento do raio. Para essa questão, declare uma variável “pi” com valor de 3,14 e
use como valor de π. Calcule também o comprimento e o diâmetro."""

pi = float(3.14)

raio = int(input('o raio da circunferencia é:'))

area = pi * (raio**2)

print('a área da circunferencia é:{}'.format(area))

comprimento = 2 * pi * raio

print('o comprimento é: {}'.format(comprimento))

diametro = raio * 2

print('o diâmetro da circunferencia é {}'.format(diametro))
