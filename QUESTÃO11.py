# 11)


def verifica_triangulo_1(a, b, c):

    resposta = ""

    if ((a + b) > c):  # primeiro 'if' a=2, b=3, c=4

        if ((b + c) > a):  # segundo 'if' só testa de (b+c) > a se o primeiro 'if' for verdadeiro

            if ((a + c) > b):  # terceiro 'if' #só testa de (a+c) > b se o segundo 'if' for verdadeiro
                # só é triângulo se os 3 'if' forem verdadeiros.
                resposta = "é triângulo"

            else:  # se o terceiro 'if' for falso
                resposta = "não é triângulo"

        else:  # se o segundo 'if' for falso
            resposta = "não é triângulo"

    else:  # se o primeiro 'if' for falso
        resposta = "não é triângulo"

    return resposta


print(verifica_triangulo_1(a=10, b=6, c=10))
# usando operador 'and'.


# def verifica_triangulo_2(a, b, c):

#     resultado = ""
#     #     TRUE                  TRUE            TRUE
#     if (((a + b) > c) and ((a + c) > b) and ((b + c) > a)):  # a=2, b=2, c=2

#         # return "triângulo"
#         resultado = "triângulo"

#     else:
#         # return "não triângulo"
#         resultado = "não triângulo"

#     return resultado
