# Indique o resultado lógico das seguintes expressões:

# a) 2 < 3


print(2 < 3)

print((6 < 3) or (10 > 11))

print(((6//3) % 2) > 5) and (2 < (3 % 2))

print(!(2 < 3))
