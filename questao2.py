"""indique qual o resultado será obtido das seguintes expressões (lembre-se de usar “.” não “,” para casas decimais): """

a = int(4*4 + 1)
print(a)

b = int(6+20-23)
print(b)

c = float(3.0 + 1)
print(c)

d = float((1/4)+2)
print(d)

e = float((29.0/7)+4)
print(e)

f = int((3/6.0)-7)
print(f)

g = int(2/2)
print(g)

h = int(2//2)
print(h)

i = int(4 % 2)
print(i)

j = int((100//5) % 5)
print(j)
